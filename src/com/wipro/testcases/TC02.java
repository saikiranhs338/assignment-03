package com.wipro.testcases;

import org.openqa.selenium.chrome.ChromeDriver;

import java.io.*;
import java.util.Properties;

import org.openqa.selenium.By;
import org.testng.annotations.*;

import com.wipro.testbase.TestBase;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class TC02 extends TestBase {
	@BeforeClass
	public void start() {
		driver=new ChromeDriver();
        driver.get("https://demo.opencart.com");
	}
	@AfterClass
	public void stop() {
		driver.close();
	}
	@Test
	public void test1() throws InterruptedException, BiffException, FileNotFoundException, IOException  {
		Properties properties=new Properties();
		properties.load(new FileInputStream("D:\\20111594\\2. OOPS concepts\\Assignment2\\config\\config.properties"));
		String url = properties.getProperty("url");
		driver.get(url);
		System.out.println(url);
		Thread.sleep(2000);
	}
	@Test
	public void test2() throws Exception{
		driver.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/a/span[2]")).click();
		driver.findElement(By.linkText("Login")).click();
		Thread.sleep(1000);
	}
	@Test(dataProvider = "SearchProvider")
	public void test3(String[] str) throws Exception{
		driver.findElement(By.name("email")).sendKeys(str[0]);
		Thread.sleep(5000);
		driver.findElement(By.name("password")).sendKeys(str[1]);
		Thread.sleep(2000);
	}
	@DataProvider(name = "SearchProvider")
	public String[] getDataFromDataProvider() throws BiffException, IOException {
		File f=new File("D:\\login.xls");
		Workbook b=Workbook.getWorkbook(f);
		Sheet s=b.getSheet(0);
		int col=s.getColumns();
		String[] str=new String[col];
		for(int i=0;i<col;i++){
			Cell c=s.getCell(i, 0);
			System.out.println(c.getContents());
			str[i] = c.getContents();
		}
		return str;
	}
}
