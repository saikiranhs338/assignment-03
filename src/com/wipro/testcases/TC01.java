package com.wipro.testcases;

import org.openqa.selenium.chrome.ChromeDriver;

import java.io.*;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.annotations.*;
import java.io.File;

import com.wipro.testbase.TestBase;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class TC01 extends TestBase{
	
	@BeforeClass
	public void start() {
		//driver.manage().window().maximize();
		driver=new ChromeDriver();
        driver.get("https://demo.opencart.com");
	}
	@AfterClass
	public void stop() {
		driver.close();
	}
	@Test
	public void test1() throws InterruptedException, BiffException, FileNotFoundException, IOException  {
		Properties properties=new Properties();
		properties.load(new FileInputStream("D:\\20111594\\2. OOPS concepts\\Assignment2\\config\\config.properties"));
		String url = properties.getProperty("url");
		driver.get(url);
		System.out.println(url);
		Thread.sleep(2000);
	}
	@Test
	public void test2() throws InterruptedException {
		 driver.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/a")).click();
	     driver.findElement(By.linkText("Register")).click();
	     Thread.sleep(2000);    
	}
	@Test
    public void test3() throws BiffException, IOException, InterruptedException {
          File f=new File("D:\\data.xls");
          Workbook b=Workbook.getWorkbook(f);
        Sheet s=b.getSheet(0);
        int col=s.getColumns();
        String []str=new String[col];
        for(int i=0;i<col;i++){
            Cell c=s.getCell(i, 0);
            str[i]=c.getContents();
        }
        driver.findElement(By.name("firstname")).sendKeys(str[0]);
        driver.findElement(By.name("lastname")).sendKeys(str[1]);
        driver.findElement(By.name("email")).sendKeys(str[2]);
        driver.findElement(By.name("telephone")).sendKeys(str[3]);
        driver.findElement(By.name("password")).sendKeys(str[4]);
        driver.findElement(By.name("confirm")).sendKeys(str[5]);
        Thread.sleep(2000);
     }
	@Test
	public void test4() throws IOException, InterruptedException{
        driver.findElement(By.xpath("//*[@id=\"content\"]/form/div/div/input[1]")).click();
        BufferedWriter writer = new BufferedWriter(new FileWriter("Policy.txt", true));
        writer.write("\nCheckbox is checked\n");
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id=\"content\"]/form/div/div/input[2]")).click();
        String st = driver.findElement(By.xpath("//*[@id=\"content\"]/h1")).getText();
        writer.write(st);
        writer.close();
        Thread.sleep(2000);
        TakesScreenshot scr = (TakesScreenshot)driver;
        File srcShot = scr.getScreenshotAs(OutputType.FILE);
        File output = new File("D:\\20111594\\2. OOPS concepts\\Assignment2\\screenshots\\output.jpg");
        FileUtils.copyFile(srcShot, output);
        Thread.sleep(2000);
    }
	@Test
	public void test5() throws IOException, InterruptedException{
		driver.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/a/span[2]")).click();
		Thread.sleep(500);
		driver.findElement(By.linkText("Logout")).click();
		BufferedWriter w = new BufferedWriter(new FileWriter("Policy.txt", true));
		w.write("\nSuccessfully Logged out\n");
		w.close();
	}
}
