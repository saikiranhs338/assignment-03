package com.wipro.testcases;

 

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

 

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

 

import com.wipro.testbase.TestBase;

 

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

 

public class TC03 extends TestBase{
    
    @BeforeClass
    public void BeforeClass() {
        driver=new ChromeDriver();
    }
    
    @Test
    public void Test1() throws IOException, InterruptedException {
        Properties properties=new Properties();
        properties.load(new FileInputStream("D:\\20111594\\2. OOPS concepts\\Assignment2\\config\\config.properties"));
        String url=properties.getProperty("url");
        driver.get(url);
        Thread.sleep(2500);
    }
    
    @Test
    public void Test2() throws InterruptedException {
        driver.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/a/i")).click();
        driver.findElement(By.linkText("Login")).click();
        Thread.sleep(2500);
    }
    
    @Test
    public void Test3() throws InterruptedException, BiffException, IOException {
        File f=new File("D:\\20111594\\2. OOPS concepts\\Assignment2\\testdata\\login.xls");
        Workbook workbook=Workbook.getWorkbook(f);
        Sheet sheet=workbook.getSheet(0);
        int col=sheet.getColumns();
        String[] str=new String[col];
        for(int i=0;i<col;i++) {
            Cell cell=sheet.getCell(i, 0);
            str[i]=cell.getContents();
        }
        driver.findElement(By.name("email")).sendKeys(str[0]);
        driver.findElement(By.name("password")).sendKeys(str[1]);
        Thread.sleep(2500);
    }
    
    @Test
    public void Test4() throws IOException, InterruptedException {
        driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/form/input")).click();
        File screenshotFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screenshotFile, new File("D:\\20111594\\2. OOPS concepts\\Assignment2\\screenshots\\output.jpg"));
        Thread.sleep(2500);
    }
    
    @Test
    public void Test5() throws InterruptedException, IOException {
        driver.findElement(By.linkText("Components")).click();
        driver.findElement(By.linkText("Monitors (2)")).click();
        File screenshotFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screenshotFile, new File("D:\\20111594\\2. OOPS concepts\\Assignment2\\screenshots\\output.jpg"));
        Thread.sleep(2500);
    }
    
    @Test
    public void Test6() throws IOException, InterruptedException {
        BufferedWriter writer = new BufferedWriter(new FileWriter("resources\\output_data\\Prize.txt", true));
        writer.write(driver.findElement(By.linkText("Apple Cinema 30\"")).getText());
        writer.write("\n"+driver.findElement(By.xpath("//*[@id=\"content\"]/div[3]/div[1]/div/div[2]/div[1]/p[2]/span[1]")).getText());
        writer.close();
        Thread.sleep(2500);
    }
    
    @Test
    public void Test7() throws InterruptedException {
        driver.findElement(By.xpath("//*[@id=\"content\"]/div[3]/div[1]/div/div[2]/div[2]/button[1]/i")).click();
        Thread.sleep(2500);
    }
    
    @Test
    public void Test8() throws InterruptedException, BiffException, IOException {
        driver.findElement(By.name("option[223][]")).click();
        File f=new File("");
        Workbook workbook=Workbook.getWorkbook(f);
        Sheet sheet=workbook.getSheet(0);
        int col=sheet.getColumns();
        String[] str=new String[col];
        for(int i=0;i<col;i++) {
            Cell cell=sheet.getCell(i, 0);
            str[i]=cell.getContents();
        }
        driver.findElement(By.name("option[208]")).sendKeys(str[0]);
        driver.findElement(By.xpath("//*[@id=\"input-option217\"]/option[4]")).click();
        driver.findElement(By.name("option[209]")).sendKeys(str[1]);
        driver.findElement(By.id("button-upload222")).sendKeys("D:\\20111594\\2. OOPS concepts\\Assignment2\\screenshots\\TC01_5.png");
        driver.findElement(By.name("option[219]")).sendKeys(str[2]);
        driver.findElement(By.name("option[221]")).sendKeys(str[3]);
        driver.findElement(By.name("option[220]")).sendKeys(str[4]);
        driver.findElement(By.name("quantity")).sendKeys(str[5]);
        driver.findElement(By.id("button-cart")).click();
        Thread.sleep(2500);
    }
    
    @AfterClass
    public void AfterClass() {
        driver.close();
    }    
}